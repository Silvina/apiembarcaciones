<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
//use App\Http\Controllers\DeportistaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResource('embarcaciones','App\Http\Controllers\EmbarcacionController');
Route::apiResource('puertos','App\Http\Controllers\PuertoController');
Route::apiResource('propietarios','App\Http\Controllers\PropietarioController');

Route::get('puertos/{puerto}/servicios','App\Http\Controllers\PuertoController@getServicios')->name('puertos.getServicios');
Route::get('puertos/{puerto}/embarcaciones','App\Http\Controllers\PuertoController@getEmbarcaciones')->name('puertos.getEmbarcaciones');
Route::get('puertos/{puerto}/historial','App\Http\Controllers\PuertoController@getHistorial')->name('puertos.getHistorial');
Route::get('propietarios/{propietario}/servicios','App\Http\Controllers\PropietarioController@getRequerimientos')->name('propietarios.getRequerimientos');


