<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puertos', function (Blueprint $table) {
            $table->id();
            //Lo ideal seria tomar desde una base de datos a parte estos datos de ciudad,pcia y pais
            //y solo almacenar los id's, por cuestion de agilizar es que guardo directamente los nombres
            $table->string('nombre');
            $table->string('ciudad');
            $table->string('provincia');
            $table->string('pais');
            $table->string('ubicacionGeografica');
            $table->string('servicios'); //Este campo se armo en una tabla a parte por la relacion muchos a muchos
            $table->string('imagen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puertos');
    }
}
