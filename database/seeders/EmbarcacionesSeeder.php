<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmbarcacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            ['matricula'=>'A100142',
            'nombre' => 'Embarcacion1',
            'tipoEmbarcacion_id' => '1',
            'propietario_id' => '1',
         
        ],

            ['matricula'=>'A100143',
            'nombre' => 'Embarcacion2',
            'tipoEmbarcacion_id' => '2',
            'propietario_id' => '2',
           
        ],

            ['matricula'=>'A100144',
            'nombre' => 'Embarcacion3',
            'tipoEmbarcacion_id' => '1',
            'propietario_id' => '1',
           
        ],
            ['matricula'=>'A100145',
            'nombre' => 'Embarcacion4',
            'tipoEmbarcacion_id' => '3',
            'propietario_id' => '2',
          
        ],
            ['matricula'=>'A100146',
            'nombre' => 'Embarcacion5',
            'tipoEmbarcacion_id' => '1',
            'propietario_id' => '3',
          
        ],
            ['matricula'=>'A100147',
            'nombre' => 'Embarcacion6',
            'tipoEmbarcacion_id' => '1',
            'propietario_id' => '2',
          
    ],
            ['matricula'=>'A100148',
            'nombre' => 'Embarcacion7',
            'tipoEmbarcacion_id' => '6',
            'propietario_id' => '3',
         
            ]
];

        \DB::table('embarcacions')->insert($datos);
    
    }
}
