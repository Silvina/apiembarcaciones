<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Seeder\DB;

class TipoEmbarcacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $datos = [
            ['descripcion' => 'Velero'],
            ['descripcion' => 'Yate'],
            ['descripcion' => 'Moto de Agua'],
            ['descripcion' => 'Barco Mercante'],
            ['descripcion' => 'Crucero'],
            ['descripcion' => 'Submarino']
        ];
        
        \DB::table('tipos_embarcaciones')->insert($datos);
       
    }


}
