<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PropietariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos =  [
            ['dni'=>'28554663',
        'nombre' => 'Mario',
        'apellido' => 'Fernandez',
        'fechaNacimiento' => '1987-03-25',
        'mail' => 'mariofernandez@gmail.com',
        'telefono' => '1125635489', 
        'imagen' => '',
        ],
        ['dni'=>'12548633',
        'nombre' => 'Fabián',
        'apellido' => 'Gimenez',
        'fechaNacimiento' => '1987-03-25',
        'mail' => 'fabiangimenez@gmail.com',
        'telefono' => '1125635236', 
        'imagen' => '',
        ],
        ['dni'=>'25478633',
        'nombre' => 'Luis',
        'apellido' => 'Montero',
        'fechaNacimiento' => '1987-03-25',
        'mail' => 'luismontero@gmail.com',
        'telefono' => '2302455663', 
        'imagen' => '',
     ]];
        
        \DB::table('propietarios')->insert($datos);
    }
}
