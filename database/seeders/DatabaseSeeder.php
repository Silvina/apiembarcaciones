<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmbarcacionesSeeder::class,
            HistorialSeeder::class,
            PropietariosSeeder::class,
            PuertosSeeder::class,
            ServiciosSeeder::class,
            propietarioServicioSeeder::class,
            TipoEmbarcacionSeeder::class,
            PuertoTipoSeeder::class,
        ]);
    }
}
