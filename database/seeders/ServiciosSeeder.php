<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            ['descripcion' => 'Limpieza exterior'],
            ['descripcion' => 'Limpieza interior'],
            ['descripcion' => 'Reparacion de motor'],
            ['descripcion' => 'Reparacion general']
        ];
        
        \DB::table('servicios')->insert($datos);
    }
}
