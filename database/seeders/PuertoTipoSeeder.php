<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class puertoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            [
                'tipo_id'=>'1',
                'puerto_id' => '1',
                'servicio_id' => '1',
                'costoServicio' => '10000',
            ],
            [
                'tipo_id'=>'5',
                'puerto_id' => '1',
                'servicio_id' => '2',
                'costoServicio' => '30000',

            ],
            [
                'tipo_id'=>'6',
                'puerto_id' => '1',
                'servicio_id' => '3',
                'costoServicio' => '35000',

            ],
            [
                'tipo_id'=>'2',
                'puerto_id' => '2',
                'servicio_id' => '3',
                'costoServicio' => '25000',

            ],
            [
                'tipo_id'=>'1',
                'puerto_id' => '2',
                'servicio_id' => '1',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'5',
                'puerto_id' => '2',
                'servicio_id' => '1',
                'costoServicio' => '40000',
            ],
            [
                'tipo_id'=>'2',
                'puerto_id' => '3',
                'servicio_id' => '3',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'3',
                'puerto_id' => '3',
                'servicio_id' => '3',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'5',
                'puerto_id' => '3',
                'servicio_id' => '1',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'6',
                'puerto_id' => '3',
                'servicio_id' => '1',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'3',
                'puerto_id' => '4',
                'servicio_id' => '2',
                'costoServicio' => '10000',
            ],
            [
                'tipo_id'=>'4',
                'puerto_id' => '4',
                'servicio_id' => '1',
                'costoServicio' => '15000',
            ],
            [
                'tipo_id'=>'5',
                'puerto_id' => '4',
                'servicio_id' => '1',
                'costoServicio' => '15000',
            ],
        ];

        \DB::table('puerto_tipos')->insert($datos);
    }

}
