<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class propietarioServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            [
                'propietario_id'=>'1',
                'servicio_id' => '1',
            ],
            [
                'propietario_id'=>'1',
                'servicio_id' => '2',
            ],
            [
                'propietario_id'=>'1',
                'servicio_id' => '3',
            ],
            [
                'propietario_id'=>'1',
                'servicio_id' => '4',
            ],
            [
                'propietario_id'=>'2',
                'servicio_id' => '4',
            ],
            [
                'propietario_id'=>'3',
                'servicio_id' => '4',
            ],
            [
                'propietario_id'=>'3',
                'servicio_id' => '2',
            ],
        ];
        
        \DB::table('propietario_servicios')->insert($datos);
    }
}
