<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PuertosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos = [
            ['nombre'=>'Puerto Buenos Aires',
            'ciudad' => 'Buenos Aires',
            'provincia' => 'Buenos Aires',
            'pais' => 'Argentina',
            'ubicacionGeografica' => '',
            'servicios' => '', 
            'imagen' => '',
            ],
            ['nombre'=>'Puerto de Rosario',
            'ciudad' => 'Rosario',
            'provincia' => 'Buenos Aires',
            'pais' => 'Argentina',
            'ubicacionGeografica' => '',
            'servicios' => '', 
            'imagen' => '',
            ],
            ['nombre'=>'Puerto Bahía Blanca',
            'ciudad' => 'Bahia Blanca',
            'provincia' => 'Buenos Aires',
            'pais' => 'Argentina',
            'ubicacionGeografica' => '',
            'servicios' => '', 
            'imagen' => '',
            ],
            ['nombre'=>'Puerto Ramallo',
            'ciudad' => 'Ramallo',
            'provincia' => 'Buenos Aires',
            'pais' => 'Argentina',
            'ubicacionGeografica' => '',
            'servicios' => '', 
            'imagen' => '',
            ],
        ];

            \DB::table('puertos')->insert($datos);
            
        }

}
