<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class historialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datos= [
            [
                'puerto_id' => '1',
                'embarcacion_id'=>'1',
                'fecha' => '2020-05-05',
            ],
            [
                'puerto_id' => '2',
                'embarcacion_id'=>'1',
                'fecha' => '2020-08-05',
            ],
            [
                'puerto_id' => '1',
                'embarcacion_id'=>'1',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '1',
                'embarcacion_id'=>'1',
                'fecha' => '2020-05-05',
            ],
            [
                'puerto_id' => '2',
                'embarcacion_id'=>'1',
                'fecha' => '2020-08-05',
            ],
            [
                'puerto_id' => '2',
                'embarcacion_id'=>'2',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '2',
                'embarcacion_id'=>'3',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '3',
                'embarcacion_id'=>'5',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '3',
                'embarcacion_id'=>'6',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '1',
                'embarcacion_id'=>'7',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '1',
                'embarcacion_id'=>'4',
                'fecha' => '2020-06-05',
            ],
            [
                'puerto_id' => '2',
                'embarcacion_id'=>'7',
                'fecha' => '2020-06-05',
            ],
        ];
        
        \DB::table('historial')->insert($datos);
    }
}
