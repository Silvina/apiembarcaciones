<?php

namespace App\Http\Controllers;

use App\Models\Propietario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Exception;
use App\Models\Embarcacion;
use App\Models\PropietarioServicio;

class PropietarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //lista todos los propietarios

        return Propietario::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
            $datos = $request->all();

            $response = DB::table('propietarios')->insert(
                [
                    'dni' => $datos['dni'],
                    'apellido' => $datos['apellido'],
                    'nombre' => $datos['nombre'],
                    'fechaNacimiento' => $datos['fechaNacimiento'],
                    'mail' => $datos['mail'],
                    'telefono' => $datos['telefono'],
                    'imagen' => $datos['imagen'],
                ]
            );

            if($response)
            return ("El Propietario se guardó correctamente");
    
    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos = $request->all();
        
        $response = DB::table('propietarios')
        ->where('id','=',$id)
        ->update(
            [
                'dni' => $datos['dni'],
                'apellido' => $datos['apellido'],
                'nombre' => $datos['nombre'],
                'fechaNacimiento' => $datos['fechaNacimiento'],
                'mail' => $datos['mail'],
                'telefono' => $datos['telefono'],
                'imagen' => $datos['imagen']
            ]
        );

        if($response)
            return ("El Propietario se actualizó correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $propietario = Propietario::find($id);

        $propietario->embarcacion()->delete();
        $propietario->PropietarioServicio()->delete();
        $ok= $propietario->delete();

        if($ok)
        return ("El Propietario se eliminó correctamente");
        
    }

     //funcion que obtiene servicios coontratados por cada propietario para su embarcacion, en que puerto y el costo de cada uno
     public function getRequerimientos($id)
     {
        $response = DB::table('propietarios')
         ->select('propietarios.nombre as Nombre',
         'propietarios.apellido as Apellido',
                    'servicios.descripcion as Servicio', 
                    'puerto_tipos.costoServicio as Costo',
                    'embarcacions.nombre as Embarcacion',                  
                    'puertos.nombre as Puerto'
                   )
          ->where('propietarios.id','=',$id)
          ->leftjoin('embarcacions','embarcacions.propietario_id','=','propietarios.id')
          ->leftjoin('propietario_servicios','propietario_servicios.propietario_id','=','propietarios.id')
          ->leftjoin('servicios','servicios.id','=','propietario_servicios.servicio_id')
          ->leftjoin('puerto_tipos','puerto_tipos.servicio_id','=','servicios.id')
          ->leftjoin('puertos','puertos.id','=','puerto_tipos.puerto_id')
          ->get();

         return  $response;
         
     }
}
