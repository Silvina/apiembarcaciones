<?php

namespace App\Http\Controllers;
use App\Models\Embarcacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EmbarcacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Lista de todas las embarcaciones
        return Embarcacion::all();
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();

        $response = DB::table('embarcacions')->insert(
            [
                'matricula' => $datos['matricula'],
                'nombre' => $datos['nombre'],
                'tipoEmbarcacion_id' => $datos['tipoEmbarcacion_id'],
                'propietario_id' => $datos['propietario_id'],
                'color' => $datos['color'],
                'largo' => $datos['largo'],
                'ancho' => $datos['ancho'],
                'pesoMaximoDeCarga' => $datos['pesoMaximoDeCarga'],
                'imagen' => $datos['imagen'],
            ]
        );


        if($response)
            return ("La embarcación se creó correctamente");
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos = $request->all();
        
        $response = DB::table('embarcacions')
        ->where('id','=',$id)
        ->update(
            [
                'matricula' => $datos['matricula'],
                'nombre' => $datos['nombre'],
                'tipoEmbarcacion_id' => $datos['tipoEmbarcacion_id'],
                'propietario_id' => $datos['propietario_id'],
                'color' => $datos['color'],
                'largo' => $datos['largo'],
                'ancho' => $datos['ancho'],
                'pesoMaximoDeCarga' => $datos['pesoMaximoDeCarga'],
                'imagen' => $datos['imagen']
            ]
        );

        if($response)
        return ("La embarcación se modificó correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $embarcacion = Embarcacion::find($id);
        $ok = $embarcacion->delete($id);
        if($ok)
        return ("La embarcación se eliminó correctamente");
    }



}
