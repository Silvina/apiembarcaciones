<?php

namespace App\Http\Controllers;
use App\Models\Puerto;
use App\Models\PuertoTipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PuertoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Lista todos los puertos
        $puertos = Puerto::all();
        return $puertos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();

        $response = DB::table('puertos')->insert(
            [
                'ciudad' => $datos['ciudad'],
                'provincia' => $datos['provincia'],
                'pais' => $datos['pais'],
                'ubicacionGeografica' => $datos['ubicacionGeografica'],
                'imagen' => $datos['imagen']
            ]
        );

        if($response)
        return ("El Puerto se creó correctamente");
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos = $request->all();
        
        $response = DB::table('puertos')
        ->where('id','=',$id)
        ->update(
            [
                'nombre' => $datos['nombre'],
                'ciudad' => $datos['ciudad'],
                'provincia' => $datos['provincia'],
                'pais' => $datos['pais'],
                'ubicacionGeografica' => $datos['ubicacionGeografica'],
                'imagen' => $datos['imagen']
            ]
        );

        if($response)
        return ("El Puerto se actualizó correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $puerto = Puerto::find($id);
        $ok = $puerto->delete($id);
        if($ok)
        return ("El Puerto se eliminó correctamente");
    }

    //funcion que obtiene los servicios que ofrece cada puerto y el costo
    public function getServicios($id)
    {
        $response = DB::table('puertos')
        ->select('tipos_embarcaciones.descripcion as Tipo','servicios.descripcion as Servicio','puertos.nombre as Puerto:','puerto_tipos.costoServicio as Costo')
        ->where('puertos.id','=',$id)
        ->join('puerto_tipos','puerto_id','=','puertos.id')
        ->join('tipos_embarcaciones','tipos_embarcaciones.id','=','puerto_tipos.tipo_id')
        ->join('servicios','servicios.id','=','puerto_tipos.servicio_id')
        ->get();

        return $response;
        
    }

     //funcion que obtiene las embarcaciones que hay en cada puerto
     public function getEmbarcaciones($id)
     {
         $response = DB::table('puertos')
         ->select('embarcacions.nombre as Embarcacion','embarcacions.matricula as Matricula','puertos.nombre as Puerto')
         ->where('puertos.id','=',$id)
         ->join('historial','puerto_id','=','puertos.id')
         ->join('embarcacions','embarcacions.id','=','historial.embarcacion_id')
         ->get();
 
         return $response;
         
     }

         //funcion que obtiene historial de las embarcaciones que estuvieron en un puerto
         public function getHistorial($id)
         {
             $response = DB::table('puertos')
             ->select('historial.fecha as Fecha','embarcacions.nombre as Embarcacion','puertos.nombre as Puerto')
             ->where('puertos.id','=',$id)
             ->join('historial','puerto_id','=','puertos.id')
             ->join('embarcacions','embarcacions.id','=','historial.embarcacion_id')
             ->get();
     
             return $response;
             
         }




}
