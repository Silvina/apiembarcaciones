<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Embarcacion extends Model
{
    use HasFactory;
    
    protected $fillable =['matricula','nombre','tipoEmbarcacion_id','propietario_id'];

    public function TipoEmbarcacion(){
        return $this->belongsTo(TipoEmbarcacion::class,'id','tipoEmbarcacion_id');
    }

    public function Historial(){
        return $this->hasMany(Historial::class,'embarcacion_id','id');
    }

    public function Propietario(){
        return $this->belongsTo(Propietario::class,'id','propietario_id');
    }
}
