<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    use HasFactory;

    public function PropietarioServicio(){
        return $this->hasMany(PropietarioServicio::class,'propietario_id','id');
    }

    public function Embarcacion(){
        return $this->hasMany(Embarcacion::class,'propietario_id','id');
    }
}
