<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    use HasFactory;

    public function PuertoTipo(){
        return $this->hasMany(PuertoTipo::class,'servicio_id','id');
    }
}
