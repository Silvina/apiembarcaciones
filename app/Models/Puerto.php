<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Puerto extends Model
{
    use HasFactory;

    public function Historial(){
        return $this->hasMany(Historial::class,'puerto_id','id');
    }

    public function PuertoTipo(){
        return $this->hasMany(PuertoTipo::class,'puerto_id','id');
    }
}
