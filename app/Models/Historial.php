<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    use HasFactory;

    public function Embarcacion(){
        return $this->belongsTo(Embarcacion::class,'id','tipoEmbarcacion_id');
       
    }

    public function Puerto(){
        return $this->belongsTo(Puerto::class,'id','tipoEmbarcacion_id');
       
    }
}
