<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuertoTipo extends Model
{
    use HasFactory;
    protected $fillable =['servicio_id','puerto_id','tipo_id'];

    public function TipoEmbarcacion(){
        return $this->belongsTo(TipoEmbarcacion::class,'id','tipoEmbarcacion_id');
    }

    public function Puerto(){
        return $this->belongsTo(Puerto::class,'id','puerto_id');
    }

    public function Servicio(){
        return $this->belongsTo(Servicio::class,'id','servicio_id');
    }
}
