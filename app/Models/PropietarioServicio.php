<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropietarioServicio extends Model
{
    use HasFactory;

    public function Propietario(){
        return $this->belongsTo(Propietario::class,'id','propietario_id');
    }
}
