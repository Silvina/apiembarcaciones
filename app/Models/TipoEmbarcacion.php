<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoEmbarcacion extends Model
{
    use HasFactory;

    public function Embarcacion(){
        return $this->hasMany(Embarcacion::class,'tipoEmbarcacion_id','id');
    }

    public function PuertoTipo(){
        return $this->hasMany(PuertoTipo::class,'tipoEmbarcacion_id','id');
    }
}
