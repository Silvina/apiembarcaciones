OBSERVACIONES Y CONSIDERACIONES:
** Se agregó la tabla puerto_tipo considerando 
	por un lado, que el costo del servicio dependera 
	del tipo de embarcacion y 
	por otro, cada puerto admite cierto tipo de embarcaciones, 
	eso tambien se ve reflejado desde esta tabla.
** En el CRUD de PUERTOS  se pide como campo requerido el servicio que ofrece, 
	pero eso se arma a parte. 
	En puerto_tipo estan los id's asociados que muestran los servicios de cada puerto
**Respecto al punto 4 de Requerimientos de analisis:
	tengo en cuenta que: cada propietario puede tener una o mas embarcaciones 
	y contratar uno o mas servicios para cada embarcacion,
	por eso se verá en el resultado el detalle de las embarcaciones con sus servicios.
**Respecto a los puntos 2 y 3 de Requerimientos de analisis:
	me resultó confuso porque eran similares, pero arme una tabla de relacion que muestre 
	el historial con la fecha en que estuvo esa embarcacion en un puerto 
**En Propietarios se hizo un borrado en cascada utilizando las relaciones con eloquent(se deberia hacer en todos lados para evitar inconsistencias)

++ Se hizo utilizacion de migraciones y seeders para crear y completar la base con datos iniciales
++ Si bien se definieron las relaciones entre las tablas con de Eloquent, no se utilizo 
(pero queria dejarlas escritas) porque 
simplemente estoy acostumbrada  a hacerlo de la manera en que lo veran resuelto. 
Tengo conocimiento de la otra manera de resolver las consultas por eso lo deje implementado. 
++ PARA CADA CRUD ACONSEJO RECARGAR LAS MIGRACIONES Y SEEDERS PARA EVITAR ERRORES. (php artisan migrate:fresh y php artisan db:seed)
++ No hice el manejo de errores como corresponde. Simplemente mostre mensajes de devolucion ante una accion realizada
++ en la carpeta collections estan los endpoints para las pruebas en postman